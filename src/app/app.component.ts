import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-blog';

  posts = [
    {
        title: "Mon premier post",
        content: "Lorem ipsum",
        loveIts: 1,
        created_at: new Date()
    },
    {
        title: "Mon second post",
        content: "Lorem ipsum",
        loveIts: 0,
        created_at: new Date()
    },
    {
      title: "Encore un post",
      content: "Lorem ipsum",
      loveIts: 0,
      created_at: new Date()
  }
  ];
}
